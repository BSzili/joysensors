
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <exec/exec.h>
#include <exec/memory.h>

#include <devices/input.h>
#include <devices/inputevent.h>
#include <libraries/sensors.h>
#include <libraries/sensors_hid.h>
#include <libraries/iffparse.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/muimaster.h>
#define USE_INLINE_STDARG
#include <proto/sensors.h>
#undef USE_INLINE_STDARG
#include <proto/alib.h>
#include <clib/debug_protos.h>
#include <proto/utility.h>

#include "plugin.h"
#include "version.h"

#define DEBUG
#ifdef DEBUG
#define D(x) x
#define bug(fmt, ...) kprintf((CONST_STRPTR)fmt, __VA_ARGS__)
#else
#define D(x)
#endif

#define BUTTON_IMAGES

#define KEYNAME "JOYSENSORS"
static char ininame[16];

static FPSEAmigaAbout info = {
	FPSE_IFACE_MAGIC,
	FPSE_IFACE_VERSION,
	FPSE_JOY,
	"joysensors",
	(char *)VERSION,
	"BSzili",
	"Sensors JOY plugin.",
};

//-----------------------------------------------------------------

static int DummyPoll(int outbyte)
{
	return ACK_OK;
}

#define ANALOG_MIN (0x00)
#define ANALOG_MAX (0xFF)
#define ANALOG_MID (0x7F)

static UINT8 *padbuf0 = NULL;
static int (*CmdPoll)(int) = DummyPoll;
static int padcnt = 0;

static int NormalPoll(int outbyte);
static int Cmd46Poll_Analog(int outbyte);
static int Cmd46Poll_Digital(int outbyte);
static int Cmd4CPoll(int outbyte);
static int FirstPoll(int outbyte);

static int JOY_Device = 0;

#define DIGITAL_UP (12)
#define DIGITAL_DOWN (14)
#define DIGITAL_LEFT (15)
#define DIGITAL_RIGHT (13)
#define DIGITAL_START (11)
#define DIGITAL_SELECT (8)
#define DIGITAL_TRIANGLE (4)
#define DIGITAL_CROSS (6)
#define DIGITAL_SQUARE (7)
#define DIGITAL_CIRCLE (5)
#define DIGITAL_L1 (2)
#define DIGITAL_L2 (0)
#define DIGITAL_R1 (3)
#define DIGITAL_R2 (1)
#define DIGITAL_L3 (9)
#define DIGITAL_R3 (10)

#define ANALOG_MODE (16)
#define ANALOG_LEFT_UP (17)
#define ANALOG_LEFT_DOWN (18)
#define ANALOG_LEFT_LEFT (19)
#define ANALOG_LEFT_RIGHT (20)
#define ANALOG_RIGHT_UP (21)
#define ANALOG_RIGHT_DOWN (22)
#define ANALOG_RIGHT_LEFT (23)
#define ANALOG_RIGHT_RIGHT (24)

#define MAX_JOYSTICKS (16)
//#define MAX_AXES (16)
//#define MAX_BUTTONS (32)
#define MAX_SENSORS (64)
#define BUFFER_SIZE (256)

#define ARRAY_SIZE(x) ((int)(sizeof(x) / sizeof(x[0])))

static struct keysym_type {
	char *label;
	int bitnum;
	APTR sensor;
	ULONG type;
} keysym[] = {
	/* Digital */
	{ "D-pad", DIGITAL_UP, NULL, SensorType_HIDInput_Unknown },
	{ "Start", DIGITAL_START, NULL, SensorType_HIDInput_Unknown },
	{ "Select", DIGITAL_SELECT, NULL, SensorType_HIDInput_Unknown },
	{ "Triangle", DIGITAL_TRIANGLE, NULL, SensorType_HIDInput_Unknown },
	{ "Cross", DIGITAL_CROSS, NULL, SensorType_HIDInput_Unknown },
	{ "Square", DIGITAL_SQUARE, NULL, SensorType_HIDInput_Unknown },
	{ "Circle", DIGITAL_CIRCLE, NULL, SensorType_HIDInput_Unknown },
	{ "L1", DIGITAL_L1, NULL, SensorType_HIDInput_Unknown },
	{ "L2", DIGITAL_L2, NULL, SensorType_HIDInput_Unknown },
	{ "R1", DIGITAL_R1, NULL, SensorType_HIDInput_Unknown },
	{ "R2", DIGITAL_R2, NULL, SensorType_HIDInput_Unknown },
	/* Analog */
	{ "Analog", ANALOG_MODE, NULL, SensorType_HIDInput_Unknown },
	{ "LeftAnalogStick", ANALOG_LEFT_UP, NULL, SensorType_HIDInput_Unknown },
	{ "LeftAnalogPress", DIGITAL_L3, NULL, SensorType_HIDInput_Unknown },
	{ "RightAnalogStick", ANALOG_RIGHT_UP, NULL, SensorType_HIDInput_Unknown },
	{ "RightAnalogPress", DIGITAL_R3, NULL, SensorType_HIDInput_Unknown },
};

//#define NUM_KEYSYM ((int)ARRAY_SIZE(keysym))
#define NUM_KEYSYM ARRAY_SIZE(keysym)

APTR child_sensors;
char current_sensor[BUFFER_SIZE];

char *va(char *fmt, ...)
{
	va_list ap;
	static char string[2][BUFFER_SIZE];
	static unsigned int index = 0;
	char *buf;

	buf = string[index & 1];
	index++;

	va_start(ap, fmt);
	vsnprintf(buf, BUFFER_SIZE, fmt, ap);
	va_end(ap);

	return buf;
}

static char *sensor_to_name(APTR sensor)
{
	char *name = NULL;

	D(bug("%s(%p)\n", __FUNCTION__, sensor));

	if (sensor) {
		GetSensorAttrTags(sensor, SENSORS_HID_Name, (IPTR)&name, TAG_DONE);
	}

	/*if (name == NULL) {
		name = "<NULL>";
	}*/

	return name;
}

static ULONG sensor_to_id(APTR sensor)
{
	ULONG id = 0;

	D(bug("%s(%p)\n", __FUNCTION__, sensor));

	if (sensor) {
		GetSensorAttrTags(sensor, SENSORS_HIDInput_ID, (IPTR)&id, TAG_DONE);
	}

	return id;
}

static char *child_sensor_type_to_name(APTR sensor)
{
	ULONG type;

	D(bug("%s(%p)\n", __FUNCTION__, sensor));

	if (sensor) {
		GetSensorAttrTags(sensor, SENSORS_Type, (IPTR)&type, TAG_DONE);
		switch (type) {
			case SensorType_HIDInput_Trigger:
				return "Button";
			case SensorType_HIDInput_Analog:
				return "Analog Button";
			case SensorType_HIDInput_Stick:
				return "Directional Pad";
			case SensorType_HIDInput_AnalogStick:
				return "Analog Stick";
			case SensorType_HIDInput_3DStick:
				return "3D Analog Stick";
		}
	}

	return "Unknown";
}

static char *child_sensor_to_name(APTR sensor)
{
	char *name = NULL;

	D(bug("%s(%p)\n", __FUNCTION__, sensor));

	if (sensor) {
		GetSensorAttrTags(sensor, SENSORS_HIDInput_Name, (IPTR)&name, TAG_DONE);
		if (name == NULL) {
			name = va("%s #%lu", child_sensor_type_to_name(sensor), sensor_to_id(sensor));
		}
	}

	return name;
}

static APTR get_sensor_by_id(APTR list, ULONG sid, BOOL fallback)
{
	APTR sensor = NULL;
	ULONG id;

	D(bug("%s(%p, %lu, %d)\n", __FUNCTION__, list, sid, fallback));

	while ((sensor = NextSensor(sensor, list, NULL))) {
		GetSensorAttrTags(sensor, SENSORS_HIDInput_ID, (IPTR)&id, TAG_DONE);
		if (id == sid) {
			break;
		}
	}

	// just grab the first available sensor
	if (sensor == NULL && fallback) {
		sensor = NextSensor(NULL, list, NULL);
	}

	return sensor;
}

static APTR get_sensor_by_name(APTR list, char *sensor_name, BOOL fallback)
{
	APTR sensor = NULL;
	char *name;

	D(bug("%s(%p, %s, %d)\n", __FUNCTION__, list, sensor_name, fallback));

	while ((sensor = NextSensor(sensor, list, NULL))) {
		name = sensor_to_name(sensor);
		if (!strcmp(sensor_name, name)) {
			break;
		}
	}

	// just grab the first available sensor
	if (sensor == NULL && fallback) {
		sensor = NextSensor(NULL, list, NULL);
	}

	return sensor;
}

static char **get_sensor_names(APTR list, char **names, int num)
{
	APTR sensor = NULL;
	int i = 0;

	D(bug("%s(%p, %p, %d)\n", __FUNCTION__, list, names, num));

	while ((sensor = NextSensor(sensor, list, NULL)) && i < num - 1) {
		names[i] = sensor_to_name(sensor);
		D(bug("found sensor: %s\n", names[i]));
		i++;
	}
	names[i] = NULL;

	return names;
}

static void close_sensor(void)
{
	ReleaseSensorsList(child_sensors, NULL);
	child_sensors = NULL;
}

static BOOL open_sensor(char *sensor_name)
{
	APTR sensors;
	APTR sensor = NULL;
	BOOL success = TRUE;

	D(bug("%s(%s)\n", __FUNCTION__, sensor_name));

	close_sensor();

	sensors = ObtainSensorsListTags(
		SENSORS_Class, SensorClass_HID,
		//SENSORS_Type, SensorType_HID_Gamepad,
		TAG_DONE);

	sensor = get_sensor_by_name(sensors, sensor_name, TRUE);
	strncpy(current_sensor, sensor_to_name(sensor), sizeof(current_sensor));

	D(bug("Opened joystick '%s'\n", current_sensor));

	child_sensors = ObtainSensorsListTags(
		SENSORS_Parent, (IPTR)sensor,
		SENSORS_Class, SensorClass_HID,
		TAG_DONE);

	ReleaseSensorsList(sensors, NULL);

	return success;
}

void read_cfg(UINT32 *par)
{
	FPSEAmiga *spu = (FPSEAmiga *)par;
	char tmp[BUFFER_SIZE];
	int i;

	D(bug("%s(%p)\n", __FUNCTION__, par));

	strcpy(ininame, KEYNAME);
	strcat(ininame, "_");
	strcat(ininame, (char *)((FPSEAmiga *)par)->PadNr);
	D(bug("reading section '%s'\n", ininame));

	if (spu->ReadCfg(ininame, "name", current_sensor) != FPSE_OK) {
		current_sensor[0] = '\0';
	} else {
		D(bug("name=%s\n", current_sensor));
	}

	open_sensor(current_sensor);

	for (i=0; i<NUM_KEYSYM; i++) {
		if (spu->ReadCfg(ininame, keysym[i].label, tmp) == FPSE_OK) {
			ULONG id;
			sscanf(tmp, "%lu", &id);
			keysym[i].sensor = get_sensor_by_id(child_sensors, id, FALSE);
			GetSensorAttrTags(keysym[i].sensor, SENSORS_Type, (IPTR)&keysym[i].type, TAG_DONE);
			D(bug("%s=%lu (%s)\n", keysym[i].label, id, child_sensor_to_name(keysym[i].sensor)));
		}
	}
}

void write_cfg(UINT32 *par)
{
	FPSEAmiga *spu = (FPSEAmiga *)par;
	char tmp[BUFFER_SIZE];
	int i;

	D(bug("%s(%p)\n", __FUNCTION__, par));

	strcpy(ininame, KEYNAME);
	strcat(ininame, "_");
	strcat(ininame, (char *)((FPSEAmiga *)par)->PadNr);
	D(bug("writing section '%s'\n", ininame));

	D(bug("name=%s\n", current_sensor);
	spu->WriteCfg(ininame, "name", current_sensor));

	for (i=0; i<NUM_KEYSYM; i++) {
		sprintf(tmp, "%lu", sensor_to_id(keysym[i].sensor));
		D(bug("%s=%s\n", keysym[i].label, tmp));
		spu->WriteCfg(ininame, keysym[i].label, tmp);
	}
}

// This Function is called when emulator starts
int JOY_Open(UINT32 *par)
{
	D(bug("%s(%p)\n", __FUNCTION__, par));

	/* Device type */
	JOY_Device = 0x41;

	read_cfg(par);

	return FPSE_OK;
}

void JOY_Close(void)
{
	D(bug("%s()\n", __FUNCTION__));

	close_sensor();
}

static int get_key(APTR list, APTR *presssensor, BOOL stick)
{
	/*static */const char *window_text;
	struct Window *window;
	int done = 0;
	APTR notifications[MAX_SENSORS];
	APTR sensor = NULL;
	int i;
	int numnotif = 0;
	ULONG windowsig, notifsig, sigs;
	ULONG type;
	struct SensorsNotificationMessage *snmsg;
	struct MsgPort *mp;

	D(bug("%s(%p, %p, %d)\n", __FUNCTION__, list, presssensor, stick));

	if (stick) {
		window_text = "Move D-pad / analog stick to any direction or press ESC to cancel.";
	} else {
		window_text = "Press desired button or press ESC to cancel.";
	}

	*presssensor = NULL; /* no key */

	if (!(mp = CreateMsgPort())) {
		return done;
	}

	while ((sensor = NextSensor(sensor, list, NULL)) && numnotif < ARRAY_SIZE(notifications)) {
		GetSensorAttrTags(sensor, SENSORS_Type, (IPTR)&type, TAG_DONE);
		D(bug("found child-sensor '%s' type %d\n", child_sensor_to_name(sensor), type));
		if (stick) {
			if (type != SensorType_HIDInput_Stick && type != SensorType_HIDInput_AnalogStick && type != SensorType_HIDInput_3DStick) {
				continue;
			}
			notifications[numnotif++] = StartSensorNotifyTags(sensor, 
				SENSORS_Notification_Destination, (IPTR)mp,
				//SENSORS_Notification_UserData, (IPTR)type,
				SENSORS_Notification_UserData, (IPTR)sensor,
				type == SensorType_HIDInput_3DStick ? SENSORS_HIDInput_Y_Index : SENSORS_HIDInput_NS_Value, 1,
				type == SensorType_HIDInput_3DStick ? SENSORS_HIDInput_X_Index : SENSORS_HIDInput_EW_Value, 1,
				TAG_DONE);
			D(bug("added stick notification for '%s'\n", child_sensor_to_name(sensor)));
		} else {
			if (type != SensorType_HIDInput_Trigger && type != SensorType_HIDInput_Analog) {
				continue;
			}
			notifications[numnotif++] = StartSensorNotifyTags(sensor, 
				SENSORS_Notification_Destination, (IPTR)mp,
				//SENSORS_Notification_UserData, (IPTR)type,
				SENSORS_Notification_UserData, (IPTR)sensor,
				SENSORS_HIDInput_Value, 1,
				TAG_DONE);
			D(bug("added button notification for '%s'\n", child_sensor_to_name(sensor)));
		}
	}
	D(bug("added %d notifications\n", numnotif));

	window = OpenWindowTags(NULL,
		WA_Title, (IPTR)"",
		WA_Flags, WFLG_NOCAREREFRESH|WFLG_DRAGBAR|WFLG_DEPTHGADGET|WFLG_CLOSEGADGET|WFLG_RMBTRAP|WFLG_GIMMEZEROZERO,
		WA_IDCMP, IDCMP_CLOSEWINDOW/*|WFLG_REPORTMOUSE*/|IDCMP_RAWKEY|IDCMP_CHANGEWINDOW,
		WA_Left, 100,
		WA_Top, 100,
		WA_Width, 100,
		WA_Height, 40,
		WA_Activate, TRUE,
		TAG_DONE);

	if (window != NULL) {
		struct IntuiMessage *imsg = NULL;
		ULONG imCode, imClass;

		/* Resize window and set pens */
		int length = TextLength(window->RPort, (STRPTR)window_text, strlen(window_text));
		int newwidth = window->BorderLeft+length+window->BorderRight+8;
		int newheight = window->BorderTop+window->IFont->tf_YSize*2+window->BorderBottom;
		ChangeWindowBox(window, window->WScreen->Width/2 - newwidth/2, window->WScreen->Height/2 - newheight/2, newwidth, newheight);
		SetAPen(window->RPort, 1);
		SetBPen(window->RPort, 0);

		windowsig = 1L << window->UserPort->mp_SigBit;
		notifsig = 1L << mp->mp_SigBit;

		/* Wait until window has been resized */
		while (done == 0) {
			/* Wait for IDCMP message */
			sigs = Wait(windowsig | notifsig);

			if (sigs & windowsig) {
				/* Check for IDCMP messages */
				while ((imsg = (struct IntuiMessage *)GetMsg(window->UserPort))) {
					imClass = imsg->Class;
					imCode = imsg->Code;

					switch(imClass) {
						case IDCMP_CHANGEWINDOW:
							Move(window->RPort, 4, window->IFont->tf_YSize);
							Text(window->RPort, (STRPTR)window_text, strlen(window_text));
							break;

						case IDCMP_RAWKEY:
							imCode &= ~IECODE_UP_PREFIX;
							if (imCode == 69) { /* escape */
								done = -1; /* cancel */
							}
							break;

						case IDCMP_CLOSEWINDOW:
							done = -1; /* cancel */
							break;
					}
					ReplyMsg((struct Message *)imsg);
				}
			}

			if (sigs & notifsig) {
				while ((snmsg = (struct SensorsNotificationMessage *)GetMsg(mp))) {
					//type = (ULONG)snmsg->UserData;
					sensor = (APTR)snmsg->UserData;
					GetSensorAttrTags(sensor, SENSORS_Type, (IPTR)&type, TAG_DONE);

					double *i, *j;

					// child sensor
					switch (type) {
						case SensorType_HIDInput_Analog:
						case SensorType_HIDInput_Trigger:
							i = (double *)GetTagData(SENSORS_HIDInput_Value, NULL, snmsg->Notifications);
							D(bug("got input %f from button '%s'\n", *i, child_sensor_to_name(sensor)));

							if (*i >= 1.0) {
								//*presssensor = snmsg->Sensor;
								*presssensor = sensor;
								done = 1; /* ok */
							}
						break;

						case SensorType_HIDInput_Stick:
						case SensorType_HIDInput_AnalogStick:
						case SensorType_HIDInput_3DStick:
							if (type == SensorType_HIDInput_3DStick) {
								i = (double *)GetTagData(SENSORS_HIDInput_Y_Index, NULL, snmsg->Notifications);
								j = (double *)GetTagData(SENSORS_HIDInput_X_Index, NULL, snmsg->Notifications);
							} else {
								i = (double *)GetTagData(SENSORS_HIDInput_NS_Value, NULL, snmsg->Notifications);
								j = (double *)GetTagData(SENSORS_HIDInput_EW_Value, NULL, snmsg->Notifications);
							}
							D(bug("got input (%f,%f) from stick '%s'\n", *i, *j, child_sensor_to_name(sensor)));

							if (*i >= 1.0 || *i <= -1.0 || *j >= 1.0 || *j <= -1.0) {
								//*presssensor = snmsg->Sensor;
								*presssensor = sensor;
								done = 1; /* ok */
							}
							break;
					}

					ReplyMsg(&snmsg->Msg);
				}
			}
		}
		CloseWindow(window);
	}

	for (i = 0; i < numnotif; i++) {
		EndSensorNotify(notifications[i], NULL);
	}
	DeleteMsgPort(mp);

	return done;
}

static int string_in_array(char *name, char **names)
{
	int i = 0;
	char **s;

	D(bug("%s(%s, %p)\n", __FUNCTION__, name, names));

	for (s = names; *s != NULL; s++) {
		if (!strcmp(*s, name))
			break;
		i++;
	}

	if (names[i] == NULL) {
		i = 0;
	}

	return i;
}

int JOY_Configure(UINT32 *par)
{
	BOOL running = TRUE;
	ULONG signals;
	APTR label, text[NUM_KEYSYM], button, app, main_group, window, ok, cancel, Oport_a;
	APTR sensors;
	//APTR child_sensors = NULL;
	char *sensor_names[MAX_JOYSTICKS];

	#define BTN_OK (32)
	#define PORT_CHANGE_A (33)

	D(bug("%s(%p)\n", __FUNCTION__, par));

	sensors = ObtainSensorsListTags(
		SENSORS_Class, SensorClass_HID,
		//SENSORS_Type, SensorType_HID_Gamepad,
		TAG_DONE);

	get_sensor_names(sensors, sensor_names, ARRAY_SIZE(sensor_names));

	read_cfg(par);

	app = ApplicationObject,
		MUIA_Application_Author, "Szil�rd Bir�",
		MUIA_Application_Base, KEYNAME,
		MUIA_Application_Title, KEYNAME,
		MUIA_Application_Version, VERSTAG,
		MUIA_Application_Copyright, "Szil�rd Bir�",
		MUIA_Application_Description, KEYNAME,
		SubWindow, window = WindowObject,
			MUIA_Window_Title, KEYNAME,
			MUIA_Window_ID, MAKE_ID('0', 'W', 'I', 'N'),
			WindowContents, VGroup,

			Child, HGroup,
				Child, TextObject,
					MUIA_Text_PreParse, "\033r",
					MUIA_Text_Contents, "Joystick",
					MUIA_Weight, 0,
					MUIA_InnerLeft, 0,
					MUIA_InnerRight, 0,
				End,

				Child, Oport_a = CycleObject,
					MUIA_Cycle_Entries, sensor_names,
					MUIA_Cycle_Active, string_in_array(current_sensor, sensor_names),
				End,
			End,

			Child, main_group = ColGroup(3), End,
				Child, HGroup,
					Child, ok = TextObject,
						ButtonFrame,
						MUIA_Background, MUII_ButtonBack,
						MUIA_Text_Contents, "Ok",
						MUIA_Text_PreParse, "\033c",
						MUIA_InputMode, MUIV_InputMode_RelVerify,
					End,
					Child, cancel = TextObject,
						ButtonFrame,
						MUIA_Background, MUII_ButtonBack,
						MUIA_Text_Contents, "Cancel",
						MUIA_Text_PreParse, "\033c",
						MUIA_InputMode, MUIV_InputMode_RelVerify,
					End,

				End,
			End,
		End,
	End;

	if (app) {
		int i;

		DoMethod(window,
			MUIM_Notify, MUIA_Window_CloseRequest, TRUE,
			app,
			2,
			MUIM_Application_ReturnID, MUIV_Application_ReturnID_Quit
		);

		DoMethod(cancel,
			MUIM_Notify, MUIA_Pressed, FALSE,
			app,
			2,
			MUIM_Application_ReturnID, MUIV_Application_ReturnID_Quit
		);

		DoMethod(ok,
			MUIM_Notify, MUIA_Pressed, FALSE,
			app,
			2,
			MUIM_Application_ReturnID, BTN_OK
		);

		DoMethod(Oport_a,
			MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime,
			app,
			2,
			MUIM_Application_ReturnID, PORT_CHANGE_A
		);

		for (i=0; i<NUM_KEYSYM; i++) {
			/* label */
			label = TextObject,
				MUIA_Text_PreParse, "\033r",
#ifdef BUTTON_IMAGES
				MUIA_Text_Contents, va("%s \33I[5:PROGDIR:plugin/%s/%s.png]", keysym[i].label, KEYNAME, keysym[i].label),
#else
				MUIA_Text_Contents, keysym[i].label,
#endif
				MUIA_InnerLeft, 0,
				MUIA_InnerRight, 0,
			End;

			text[i] = TextObject,
				MUIA_Background, MUII_TextBack,
				MUIA_Frame, MUIV_Frame_Text,
				MUIA_Text_Contents, child_sensor_to_name(keysym[i].sensor),
			End;

			button = TextObject,
				ButtonFrame,
				MUIA_Background, MUII_ButtonBack,
				MUIA_Text_Contents, "Change",
				MUIA_Text_PreParse, "\033c",
				MUIA_InputMode, MUIV_InputMode_RelVerify,
			End;

			DoMethod(button,
				MUIM_Notify, MUIA_Pressed, FALSE,
				app,
				2,
				MUIM_Application_ReturnID, (i + 1)
			);

			/* add to window */
			DoMethod(main_group, OM_ADDMEMBER, label);
			DoMethod(main_group, OM_ADDMEMBER, text[i]);
			DoMethod(main_group, OM_ADDMEMBER, button);
		}

		set(window, MUIA_Window_Open, TRUE);
		while (running) {
			unsigned long retval = DoMethod(app, MUIM_Application_Input, &signals);
			switch (retval) {
				case MUIV_Application_ReturnID_Quit:
					running = FALSE;
					break;

				case BTN_OK:
					write_cfg(par);
					running = FALSE;
					break;

				case PORT_CHANGE_A: {
					ULONG active;
					get(Oport_a, MUIA_Cycle_Active, &active);

					open_sensor(sensor_names[active]);

					for (i=0; i<NUM_KEYSYM; i++) {
						keysym[i].sensor = NULL;
						keysym[i].type = SensorType_HIDInput_Unknown;
						set(text[i], MUIA_Text_Contents, "");
					}
					break;
				}

				default:
					if ((retval >= 1) && (retval <= (1+NUM_KEYSYM))) {
						int index = retval - 1;
						BOOL stick = FALSE;
						APTR sensor = NULL;

						if (keysym[index].bitnum == DIGITAL_UP ||
							keysym[index].bitnum == ANALOG_LEFT_UP ||
							keysym[index].bitnum == ANALOG_RIGHT_UP) {
							stick = TRUE;
						}

						if (get_key(child_sensors, &sensor, stick) > 0) {
							char *name;
							keysym[index].sensor = sensor;
							GetSensorAttrTags(keysym[index].sensor, SENSORS_Type, (IPTR)&keysym[index].type, TAG_DONE);
							name = child_sensor_to_name(keysym[index].sensor);
							set(text[index], MUIA_Text_Contents, name);
							D(bug("got sensor: '%s'\n", name));
						}
					}
					break;

			}
			if (running && signals) {
				Wait(signals);
			}
		}
		MUI_DisposeObject(app);
	}

	#undef PORT_CHANGE
	#undef BTN_OK

	close_sensor();
	ReleaseSensorsList(sensors, NULL);

	return FPSE_OK;
}

void JOY_About(UINT32 *par)
{
	memcpy(par, &info, sizeof(FPSEAmigaAbout));
}

void JOY_LoadState(JOY_State *state) {}
void JOY_SaveState(JOY_State *state) {}

int JOY_SetAnalog(int mode)
{
	int prev = (JOY_Device != 0x41);

	if (mode == 1) {
		JOY_Device = 0x73;
	} else if (mode == 0) {
		JOY_Device = 0x41;
	}

	return prev;
}

void JOY_SetOutputBuffer(UINT8 *buf)
{
	padbuf0 = buf;
}

int JOY_StartPoll(void)
{
	UINT32 ret = 0xffffffff;
	int lax = ANALOG_MID;
	int lay = ANALOG_MID;
	int rax = ANALOG_MID;
	int ray = ANALOG_MID;
	int i;
	double val, ns, ew;

	for (i=0; i<NUM_KEYSYM; i++) {
		switch (keysym[i].type) {
			case SensorType_HIDInput_Analog:
			case SensorType_HIDInput_Trigger:
				GetSensorAttrTags(keysym[i].sensor, SENSORS_HIDInput_Value, (IPTR)&val, TAG_DONE);
				if (val == 1.0) { // will this work with analog triggers?
					ret &= ~(1<<keysym[i].bitnum);
				}
			break;

			case SensorType_HIDInput_Stick:
			case SensorType_HIDInput_AnalogStick:
			case SensorType_HIDInput_3DStick:
				if (keysym[i].type == SensorType_HIDInput_3DStick) {
					GetSensorAttrTags(keysym[i].sensor, SENSORS_HIDInput_Y_Index, (IPTR)&ns, SENSORS_HIDInput_X_Index, (IPTR)&ew, TAG_DONE);
				} else {
					GetSensorAttrTags(keysym[i].sensor, SENSORS_HIDInput_NS_Value, (IPTR)&ns, SENSORS_HIDInput_EW_Value, (IPTR)&ew, TAG_DONE);
				}

				// Why Black Dynamite, why?
				if (keysym[i].bitnum == DIGITAL_UP) {
					if (ns == -1.0) {
						ret &= ~(1<<keysym[i].bitnum);
					} else if (ns == 1.0) {
						ret &= ~(1<<(keysym[i].bitnum + 2));
					}

					if (ew == -1.0) {
						ret &= ~(1<<(keysym[i].bitnum + 3));
					} else if (ew == 1.0) {
						ret &= ~(1<<(keysym[i].bitnum + 1));
					}
				} else {
					if (ns == -1.0) {
						ret &= ~(1<<keysym[i].bitnum);
					} else if (ns == 1.0) {
						ret &= ~(1<<(keysym[i].bitnum + 1));
					}

					if (ew == -1.0) {
						ret &= ~(1<<(keysym[i].bitnum + 2));
					} else if (ew == 1.0) {
						ret &= ~(1<<(keysym[i].bitnum + 3));
					}
				}

				// hack for the analog sticks
				if (keysym[i].bitnum == ANALOG_LEFT_UP) {
					lax = ANALOG_MID + (ew * ANALOG_MID);
					lay = ANALOG_MID + (ns * ANALOG_MID);
				} else if (keysym[i].bitnum == ANALOG_RIGHT_UP) {
					rax = ANALOG_MID + (ew * ANALOG_MID);
					ray = ANALOG_MID + (ns * ANALOG_MID);
				}
				break;
			/*default:
				break;*/
		}
	}

	padbuf0[1] = JOY_Device;
	padbuf0[2] = 0x5A;
	padbuf0[3] = ret >> 8;
	padbuf0[4] = ret;
	padbuf0[5] = rax;
	padbuf0[6] = ray;
	padbuf0[7] = lax;
	padbuf0[8] = lay;
	padcnt = 0;

	CmdPoll = FirstPoll;

	return ACK_OK;
}

static int NormalPoll(int outbyte)
{
	if (padcnt > (1+((JOY_Device&0xF)<<1)) ) return ACK_ERR;
	return ACK_OK;
}

static int DeviceSelectPoll(int outbyte)
{
	if (padcnt == 3) {
		if (outbyte == 0) {
			JOY_Device = 0x41;
		} else {
			JOY_Device = 0x73;
		}
	}
	return ACK_OK;
}

static int Cmd46Poll_Digital(int outbyte)
{
	if (padcnt == 3) {
		UINT32 *buf32 = (UINT32 *)padbuf0;
		if (outbyte) {
			buf32[1] = C_SWAP32(0x01010100);
			padbuf0[8] = 0x14;
		} else {
			buf32[1] = C_SWAP32(0x00020100);
			padbuf0[8] = 0x0A;
		}
		CmdPoll = NormalPoll;
	}
	return ACK_OK;
}

static int Cmd46Poll_Analog(int outbyte)
{
	if (padcnt == 3) {
		UINT32 *buf32 = (UINT32 *)padbuf0;
		if (outbyte) {
			buf32[1] = C_SWAP32(0x01000100);
			padbuf0[8] = 0x14;
		} else {
			buf32[1] = C_SWAP32(0x00000100);
			padbuf0[8] = 0x0A;
		}
		CmdPoll = NormalPoll;
	}
	return ACK_OK;
}
static int Cmd4CPoll(int outbyte)
{
	if (padcnt == 3) {
		if (outbyte)
			padbuf0[6] = 0x07;
		else
			padbuf0[6] = 0x04;
		CmdPoll = NormalPoll;
	}
	return ACK_OK;
}

static int FirstPoll(int outbyte)
{
	UINT32 *buf32;

	switch (outbyte) {
	case 0x44:
		buf32 = (UINT32 *)padbuf0;
		buf32[0] = C_SWAP32(0x005AF3FF);
		buf32[1] = C_SWAP32(0x00000000);
		padbuf0[8] = 0;
		CmdPoll = DeviceSelectPoll;
		break;
	case 0x45:
		buf32 = (UINT32 *)padbuf0;
		buf32[0] = C_SWAP32(0x035AF3FF);
		if (JOY_Device == 0x41)
			buf32[1] = C_SWAP32(0x01020002);
		else
			buf32[1] = C_SWAP32(0x01000102);
		padbuf0[8] = 0;
		CmdPoll = NormalPoll;
		break;
	case 0x46:
		buf32 = (UINT32 *)padbuf0;
		buf32[0] = C_SWAP32(0x005AF3FF);
		if (JOY_Device == 0x41)
			CmdPoll = Cmd46Poll_Digital;
		else
			CmdPoll = Cmd46Poll_Analog;
		break;
	case 0x47:
		buf32 = (UINT32 *)padbuf0;
		buf32[0] = C_SWAP32(0x005AF3FF);
		buf32[1] = C_SWAP32(0x01000200);
		padbuf0[8] = 0;
		CmdPoll = NormalPoll;
		break;
	case 0x4C:
		buf32 = (UINT32 *)padbuf0;
		buf32[0] = C_SWAP32(0x005AF3FF);
		buf32[1] = C_SWAP32(0x00000000);
		padbuf0[8] = 0;
		CmdPoll = Cmd4CPoll;
		break;
	case 0x4D:
		buf32 = (UINT32 *)padbuf0;
		buf32[0] = C_SWAP32(0xFF5AF3FF);
		buf32[1] = C_SWAP32(0xFFFFFFFF);
		padbuf0[8] = 0xFF;
	default:
		CmdPoll = NormalPoll;
		break;
	}
	return ACK_OK;
}

int JOY_Poll(int outbyte)
{
	padcnt++;
	return CmdPoll(outbyte);
}
