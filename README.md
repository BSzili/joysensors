# README #

FPSE input plugin using sensors.library on MorphOS

### Building ###

* Download and extract the [FPSE Plugin SDK](http://www.amidog.se/amiga/bin/FPSE/20151029/FPSE-0.10.6-SDK.tar.gz)
* Clone the git repository there into a subdirectory called `joysensors`
* In the `joysensors` subdirectory issue the `make OSMAKE=../makes/make.mos` command
