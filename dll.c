/*
*/

#include "dll/dll.h"
#include <stdio.h>

#include "plugin.h"

DLL_SYMBOLS_DECL_BEGIN()
  DLL_SYMBOL_DECL(JOY_Open)
  DLL_SYMBOL_DECL(JOY_Close)
  DLL_SYMBOL_DECL(JOY_SetOutputBuffer)
  DLL_SYMBOL_DECL(JOY_StartPoll)
  DLL_SYMBOL_DECL(JOY_Poll)
  DLL_SYMBOL_DECL(JOY_SetAnalog)
  DLL_SYMBOL_DECL(JOY_LoadState)
  DLL_SYMBOL_DECL(JOY_SaveState)
  DLL_SYMBOL_DECL(JOY_Configure)
  DLL_SYMBOL_DECL(JOY_About)
DLL_SYMBOLS_DECL_END()

#define LIBAUTO_GRAPHICS  1
#define LIBAUTO_INTUITION 1
#define LIBAUTO_MUIMASTER 1
#define LIBAUTO_SENSORS   1
#include "dll/libauto.h"

int dll_setup(void)
{
	if (libauto_open() == LIBAUTO_OK)
		return DLL_RESULT_OK;
	else
		return DLL_RESULT_SETUP_FAILED;
}

void dll_cleanup(void)
{
	libauto_close();
}
